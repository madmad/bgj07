import ecs
from Constants import *

class AmbientSound(ecs.Component):
    def __init__(self, sound=""):
        self.sound = sound

class Sound(ecs.Component):
    def __init__(self, sound="", loop=True):
        self.sound = sound
        self._playing = False
        self.loop = loop
        
class Music(ecs.Component):
    def __init__(self, song=""):
        self.song = song

class Physics(ecs.Component):
    def __init__(self, x=0, y=0, dx=0, dy=0):
        self.x  = x
        self.y  = y
        self.dx = dx
        self.dy = dy
        self.rotation = MOVE_STOP
        self.walk = False

class Destination(ecs.Component):
    def __init__(self, x, y, speed = 1):
        self.x = x
        self.y = y
        self.speed = speed
        
class Visual(ecs.Component):
    """ an animation """
    def __init__(self, look=""):
        self.look = look

class StaticImage(ecs.Component):
    """ just a static image loaded from file file at position pos """
    def __init__(self, file="", pos=(0,0), gravity=CENTER):
        self.file = file
        self.pos = pos
        self.gravity = gravity

class OnScreenText(ecs.Component):
    def __init__(self, text="Fraaaaanz!", color=(0,255,0), x=512, y=384):
        self.text = text
        self.color = color
        self.x = x
        self.y = y
        
class Effect(ecs.Component):
    """ a visual effect """
    def __init__(self, effect="", size=64, duration=1, image="roachy_hungry.png"):
        self.effect = effect
        self.duration = duration
        self.size = size
        self.image = image

class EventHandler(ecs.Component):
    def __init__(self, event, cbHndl):
        self.event = event
        self.cbHndl = cbHndl

class TTL(ecs.Component):
    def __init__(self, ttl):
        self.ttl = ttl

class Active(ecs.Component):
    """ this entity is the currently active one, for player interaction """
    pass

class Map(ecs.Component):
    """ The loaded map """
    def __init__(self, tiles):
        self.map = tiles

class Mold(ecs.Component):
    def __init__(self):
        self.growthmap = None

class Bug(ecs.Component):
    def __init__(self):
        self.speed = 7
        self.food = 30
        self.max_food = 100
        self.eating = False

class Transient(ecs.Component):
    """ This entity will be deleted at the next state switch """
    def __init__(self):
        pass

class Showerhead(ecs.Component):
    def __init__(self):
        self.ticks_without_move = 0
        self.max_twm = 70
        self.slow = 7
        self.fast = 12
        self.locked = False
        self.start_x = 512
        self.start_y = 192
        self.hunting = False
        self.locking_distance = 200
        self.wait_after_shower = 70
        self.wait = False

class Wet(ecs.Component):
    def __init__(self, tile):
        self.tile = tile
        self.slow_count = 0
        self.ticks_per_refill = 30
        self.food_used = 0

class Poison(ecs.Component):
    def __init__(self, x, y):
        #self.tile = tile
        self.duration = 500
        self.x = x
        self.y = y

class Score(ecs.Component):
    def __init__(self):
        self.score = 0

class FoodIndicator(ecs.Component):
    def __init__(self, tile):
        self.tile = tile
        self.food = 0