import ecs
from Components import *
from Events import *

class GrowthSystem(ecs.System):
    def __init__(self):
        pass

    def grow(self, foo, dt):
        pass
        
    def update(self, dt, en):
        things = en.pairs_for_type(Mold)
        if things == None: return
        for (e, c) in things:
            c.growthmap = self.grow(c.growthmap, dt)
            
