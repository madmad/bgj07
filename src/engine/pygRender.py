import pygame
from Components import *
from Constants import *
import ecs
import tmx
import math

class AnimationState(ecs.Component):
    def __init__(self, animation):
        self.animation = animation
        self.state = 0
        self.t = 0

class EffectState(ecs.Component):
    def __init__(self, animation="rainbowSparkle"):
        self.animation = animation
        self.state = 0
        self.t = 0

class pygRenderer(ecs.System):
    def __init__(self):
        self.screen = pygame.display.set_mode((1024, 768))

        self.font = pygame.font.Font(pygame.font.get_default_font(), 16)
        self.smallfont = pygame.font.Font(pygame.font.get_default_font(), 10)

        self.images = {}
#        self.overlay = tmx.Tileset.load('assets/overlay.tsx')
        
        self.animations = {
            'roach hungry': {
                'delay': 0,
                'loop': True,
                'z': 0,
                'sprites': ['roachy_hungry.png']
            },
            'tub': {
                'delay': 0,
                'loop': True,
                'z': 0,
                'sprites': ['gregs_tub.png']
            },
            'roach other': {
                'delay': 0,
                'loop': True,
                'z': 0,
                'sprites': ['roachy_yrgnuh.png']
            },            
            'roach':     { 'delay': 0,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach_phase 0.png"]},
            'roach-up':     { 'delay': 0,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-up_phase 0.png"]},
            'roach-down':     { 'delay': 0,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-down_phase 0.png"]},
            'roach-left':     { 'delay': 0,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-left_phase 0.png"]},
            'roach-right':     { 'delay': 0,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-right_phase 0.png"]},
            'roach-up-walk':     { 'delay': 150,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-up_phase 0.png","roach-up_phase 1.png","roach-up_phase 0.png","roach-up_phase 2.png"]},
            'roach-left-walk':     { 'delay': 150,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-left_phase 0.png","roach-left_phase 1.png","roach-left_phase 0.png","roach-left_phase 2.png"]},
            'roach-right-walk':     { 'delay': 150,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-right_phase 0.png","roach-right_phase 1.png","roach-right_phase 0.png","roach-right_phase 2.png"]},
            'roach-down-walk':     { 'delay': 150,
                                  'loop': True,
                                  'z': 3,
                                  'sprites': ["roach-down_phase 0.png","roach-down_phase 1.png","roach-down_phase 0.png","roach-down_phase 2.png"]},
            'showerhead':     { 'delay': 150,
                                  'loop': True,
                                  'z': 4,
                                  'sprites': ["shower_head.png"]},
            'wet':     { 'delay': 150,
                                  'loop': True,
                                  'z': 4,
                                  'sprites': ["drops_good.png"]},
            'poisoned':     { 'delay': 150,
                                  'loop': True,
                                  'z': 4,
                                  'sprites': ["drops_baad.png"]},
        }
        
        
        for _,a in self.animations.items():
            for s in a['sprites']:
                self.images[s] = pygame.image.load('assets/'+s).convert_alpha()

    def updateFoodIndicators(self, dt, entities):
        pairs = entities.pairs_for_type(FoodIndicator)
        if not pairs: return
        for (e, c) in pairs:
            color = float(c.food) / 30 * 255
            ind = self.images['drops_good.png'].copy()
            ind.fill((0, color, color))
            self.screen.blit(ind, (c.tile.px, c.tile.py), special_flags=pygame.BLEND_RGB_ADD)
                
    def update(self, dt, entities):
#        self.screen.fill((0,26,0))
        
        try:
            self.tilemap.update(dt / 1000., self)
            self.tilemap.layers['map'].draw(self.screen)
        except AttributeError:
            pass

        self.updateFoodIndicators(dt, entities)        
        self.updateAnimations(dt, entities)

        try:        
            self.tilemap.layers['doodads'].draw(self.screen)
        except AttributeError:
            pass

        self.updateGFX(dt, entities)
        self.updateUI(dt, entities)        
        pygame.display.flip()

    def load_map(self, path):
        self.tilemap = tmx.load('assets/'+path, (300, 200))
        self.tilemap.layers['map'].set_view(0,0,1024,768)
        self.tilemap.layers['doodads'].set_view(0,0,1024,768)        
        return self.tilemap

    def updateGFX(self, dt, entities):
        pairs = entities.pairs_for_type(StaticImage)
        if pairs:
            for (e, c) in pairs:
                try:
                    img = self.images[c.file]
                    
                except KeyError:
                    # if the image isn't loaded yet load it
                    img = pygame.image.load('assets/' + c.file).convert_alpha()
                    self.images[c.file] = img
                    
                (x,y) = c.pos
                if c.gravity == CENTER:
                    x -= img.get_width()/2
                    y -= img.get_height()/2
                self.screen.blit(img, (x, y))
                
        pairs = entities.pairs_for_type(Effect)
        if pairs:
            for (e, c) in pairs:
                pos = entities.component_for_entity(e, Physics)
                ef =  entities.component_for_entity(e, EffectState)
                if not ef:
                    ef = EffectState()
                    entities.add_component(e, ef)
                if self.rainbowSparkle(dt, pos, ef, c.size, 1000.0 * c.duration, c.image):
                    entities.remove_component(e, EffectState)
                    entities.remove_component(e, Effect)
        
    def change_alpha(self, surface, alpha=0.5):
        for a in pygame.surfarray.pixels_alpha(surface):
            a *= alpha
        return surface        
        
    def updateUI(self, dt, entities):
        pairs = entities.pairs_for_type(OnScreenText)
        if pairs != None:
            for (e, c) in pairs:
                text = self.font.render(c.text, True, c.color)
                self.screen.blit(text, (c.x, c.y))

        pairs = entities.pairs_for_type(Bug)
        if pairs == None: return # no bugs left!?
        (e, bug) = pairs[0]

        s = self.images['gregs_tub.png']
        x = (1024 - s.get_width())/2
        y = 9 * 64
        self.screen.blit(s, (x,y))

        # draw the hunger-meter
        x = 10
        sempty = self.images['roachy_hungry.png']
        sfull = self.images['roachy_yrgnuh.png']
        fill_ammount = bug.food / float(bug.max_food)
        self.screen.blit(sempty, (x,y))
        self.screen.blit(sfull, (x,y), area=(0,0,100,34+fill_ammount * 111)) 
        
    def updateSelection(self, dt, entities):
        pairs = entities.pairs_for_type(Active)
        if pairs == None: return
        for (e, c) in pairs:
            p = entities.component_for_entity(e, Physics)
            if not p: continue
            pygame.draw.rect(self.screen, (0,250,0), (p.x, p.y, 32,32), 1)
                
    def updateAnimations(self, dt, entities):
        surfaces = []
        pairs = entities.pairs_for_type(Visual)
        if pairs == None: return
        for (e, c) in pairs:
            p = entities.component_for_entity(e, Physics)
            if not p: continue
            anim = entities.component_for_entity(e, AnimationState)            
            if not anim:
                anim = AnimationState(c.look)
                entities.add_component(e, anim)

            if p.rotation != 0:
                anim.animation = c.look + "-"+DIRECTION[p.rotation]
                if p.walk: anim.animation += "-walk"
            else:
                anim.animation = c.look

            i = anim.state
            z = self.animations[anim.animation]['z']
            try:
                surfaces.append((z,self.images[self.animations[anim.animation]['sprites'][i]], (p.x, p.y)))
            except IndexError:
                surfaces.append((z,self.images[self.animations[anim.animation]['sprites'][0]], (p.x, p.y)))                
            anim.t += dt
            if anim.t > self.animations[anim.animation]['delay']:
                anim.t = anim.t - self.animations[anim.animation]['delay'] 
                anim.state = (anim.state + 1) 
                if anim.state >= len(self.animations[anim.animation]['sprites']):
                    if self.animations[anim.animation]['loop']:
                        anim.state=0
                    else:
                        entities.remove_component(e, Visual)
                        entities.remove_component(e, AnimationState)
        for (z, a, (x,y)) in sorted(surfaces):
            self.screen.blit(a, (x, y))

            
    def rainbowSparkle(self, dt, position, state, esize, etime, image):
        x = position.x
        y = position.y
        size = math.trunc(max(esize * state.t / etime, 1))
        surf = self.images[image]
        big = pygame.transform.smoothscale(surf, (size, size))
        big = self.change_alpha(big, min(1, 1-(state.t/etime)))
        self.screen.blit(big,(x-size/2,y-size/2))
        state.t += dt
        return state.t > etime

    def blurSurf(self, surface, amt):
        """
        Blur the given surface by the given 'amount'.  Only values 1 and greater
        are valid.  Value 1 = no blur.
        """
        if amt < 1.0:
            raise ValueError("Arg 'amt' must be greater than 1.0, passed in value is %s"%amt)
        scale = 1.0/float(amt)
        surf_size = surface.get_size()
        scale_size = (int(surf_size[0]*scale), int(surf_size[1]*scale))
        surf = pygame.transform.smoothscale(surface, scale_size)
        surf = pygame.transform.smoothscale(surf, surf_size)
        return surf
            
