EVT_STATE  = 1

MOVE_STOP  = 0
MOVE_LEFT  = 1
MOVE_RIGHT = 2
MOVE_DOWN  = 3
MOVE_UP    = 4

DIRECTION = {
    MOVE_UP: 'up',
    MOVE_LEFT: 'left',
    MOVE_DOWN: 'down',
    MOVE_RIGHT: 'right',    
}

CENTER = 0
BOTTOMRIGHT = 1
