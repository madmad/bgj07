#!/usr/bin/env python2

import pdb
import ecs
import pygame
from engine.pyg import pygameEngine
from Components import *
from Constants import *

from math import sqrt, pow
import random
from copy import deepcopy
import engine.tmx as tmx
from random import randint

class TTLSystem(ecs.System):
    def update(self, dt, en):
        things = en.pairs_for_type(TTL)
        if things == None: return
        for (e, c) in things:
            c.ttl -= dt
            if c.ttl < 0: en.delete_entity(e)
        
class EventSystem(ecs.System):
    def __init__(self, entities, game):
        self.entities = entities
        e = entities.new_entity()
        entities.add_component(e, EventHandler(pygame.QUIT, self.handleQuitEvent))
        self.game = game

    def update(self, dt, ent):
        for event in pygame.event.get():
            pairs = self.entities.pairs_for_type(EventHandler)
            if pairs == None: return
            for (e, c) in pairs:
                if type(c.event) == list:
                    if event.type in c.event:
                        c.cbHndl(self.entities, event)
                elif c.event == event.type:
                    c.cbHndl(self.entities, event)

    def handleQuitEvent(self, entities, event):
        pass

class GoalSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Bug)
        if pairs == None: return # no bugs left!?
        themaps = entities.pairs_for_type(Map)
        if themaps == None: return
        (e2, mappus) = themaps[0]
        themap = mappus.map
        for (e, bug) in pairs:
            p = entities.component_for_entity(e, Physics)
            if bug.food <= 0:
                p.dy = 10
                entities.add_component(bug, Sound('die', loop=False))
                pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))
                
class WetSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Wet)
        if pairs == None: return # nothings wet, buy lube
        for pair in pairs:
            (e, c) = pair
            if not 'current_food' in c.tile:
                if c.slow_count>c.ticks_per_refill:
                    c.food_used += 1
                    c.slow_count = 0
                    if(c.food_used>20):
                        entities.remove_component(e, Wet)
                        entities.remove_component(e, Visual)
                        entities.remove_component(e, Physics)
                else:
                    c.slow_count += 1

            else:
                if c.tile["current_food"] < c.tile["base_food"] and c.food_used < c.tile["base_food"]:
                    if c.slow_count>c.ticks_per_refill:
                        c.tile["current_food"] += 1
                        c.food_used += 1
                        c.slow_count = 0
                    else:
                        c.slow_count += 1
                else:
                    entities.delete_entity(e)
        pass

class PoisonSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Poison)

        if (pairs == None or len(pairs)<10) and randint(1,100)<2:
            x = randint(0, 15) * 64
            y = randint(0, 8) * 64
            e = entities.new_entity()
            entities.add_component(e, Poison(x, y))
            entities.add_component(e, Visual('poisoned'))
            entities.add_component(e, Physics(x, y))

        if pairs == None: return

        for (e, c) in pairs:
            bug_pairs = entities.pairs_for_type(Bug)
            if bug_pairs != None:
                (bug_e, bug_c) = bug_pairs[0]
                bug_p = entities.component_for_entity(bug_e, Physics)
                if bug_p.x == c.x and bug_p.y == c.y:
                    bug_c.food -= 1
                c.duration -= 1
                if c.duration<=0:
                    entities.delete_entity(e)
      

class ShowerMovement(ecs.System):
    def __init__(self):
        pass

    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Showerhead)
        if pairs == None: return # no shower left!?
        (me_e, me_c) = pairs[0]
        me_p = entities.component_for_entity(me_e, Physics)
        me_d = entities.component_for_entity(me_e, Destination)

        if me_c.wait:
            me_c.wait -= 1
            if me_c.wait<1:
                entities.add_component(me_e, Destination(me_c.start_x, me_c.start_y, me_c.fast))
                me_c.wait = False
            pass

        pairs = entities.pairs_for_type(Bug)
        if pairs == None: return # no bugs left!?
        (bug_e, bug_c) = pairs[0]
        bug_p = entities.component_for_entity(bug_e, Physics)
        bug_d = entities.component_for_entity(bug_e, Destination)

        if not bug_d:
            if not me_c.hunting:
                me_c.ticks_without_move += 1
        else:
            if not me_c.locked and me_c.hunting:
                entities.remove_component(me_e, Destination)
                entities.add_component(me_e, Destination(me_c.start_x, me_c.start_y, me_c.fast))
                me_c.hunting = False

        if me_c.ticks_without_move > me_c.max_twm and not me_c.hunting:
            me_c.ticks_without_move = 0
            entities.add_component(me_e, Destination(bug_p.x, bug_p.y, me_c.slow))
            me_d = entities.component_for_entity(me_e, Destination)
            me_c.hunting = True

        if me_c.hunting and me_c.locked and not me_d:
            me_c.hunting = False
            me_c.locked = False
            me_c.wait = me_c.wait_after_shower
            # shower!
            #  - remove poison if any
            #  - kill roach if any
            themaps = entities.pairs_for_type(Map)
            if themaps != None:
                (e2, mappus) = themaps[0]
                themap = mappus.map
                tile = themap.get_at(me_p.x, me_p.y)
                if bug_p.x == me_p.x and bug_p.y == me_p.y:
                    bug_c.food = 0
                #if 'current_food' in tile:
                e = entities.new_entity()
                entities.add_component(e, Wet(tile))
                entities.add_component(e, Visual('wet'))
                entities.add_component(e, Physics(me_p.x, me_p.y))
                entities.add_component(e, Sound('splash', loop=False))
            pass

        dist = sqrt((bug_p.x-me_p.x)**2 + (bug_p.y-me_p.y)**2)
        if dist < me_c.locking_distance and me_c.hunting and not me_c.locked:
            me_c.locked = True
            if me_d:
                dx = me_d.x
                dy = me_d.y
                entities.remove_component(me_e, Destination)
                entities.add_component(me_e, Destination(dx, dy, me_c.fast))

class UserMovement(ecs.System):
    def __init__(self):
        self.move = MOVE_STOP
        self.keydown = False

    def getCallbacks(self):
        return [
            EventHandler(pygame.KEYDOWN, self.handleKeyDown),
            EventHandler(pygame.KEYUP, self.handleKeyUp)
        ]

    def handleKeyDown(self, entitites, event):
        if event.key == pygame.K_UP:
            self.move = MOVE_UP
            self.keydown = True
        elif event.key == pygame.K_DOWN:
            self.keydown = True
            self.move = MOVE_DOWN
        elif event.key == pygame.K_LEFT:
            self.keydown = True
            self.move = MOVE_LEFT
        elif event.key == pygame.K_RIGHT:
            self.keydown = True
            self.move = MOVE_RIGHT
        elif event.key == pygame.K_SPACE:
            self.keydown = False
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE, 'state': 'pause'}))
            
    def handleKeyUp(self, entitites, event):
        if not self.keydown: return
        if self.move == MOVE_UP:
            if event.key == pygame.K_UP:
                self.move = MOVE_STOP
                self.keydown = False
        if self.move == MOVE_DOWN:
            if event.key == pygame.K_DOWN:
                self.move = MOVE_STOP
                self.keydown = False
        if self.move == MOVE_LEFT:
            if event.key == pygame.K_LEFT:
                self.move = MOVE_STOP
                self.keydown = False
        if self.move == MOVE_RIGHT:
            if event.key == pygame.K_RIGHT:
                self.move = MOVE_STOP
                self.keydown = False
            
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Bug)
        if pairs == None: return # no bugs left!?
        (e, c) = pairs[0]
        themaps = entities.pairs_for_type(Map)
        if themaps == None: return
        (e2, mappus) = themaps[0]
        themap = mappus.map
        p = entities.component_for_entity(e, Physics)
        d = entities.component_for_entity(e, Destination)
        if not d:
            if self.move == MOVE_UP:
                if p.y > 63 and not 'blocked' in themap.get_at(p.x, p.y - 64):
                    entities.add_component(e, Destination(p.x, p.y - 64, c.speed))
                p.rotation = MOVE_UP
                p.walk = True
            elif self.move == MOVE_DOWN:
                if p.y < 642 and not 'blocked' in themap.get_at(p.x, p.y + 64):
                    entities.add_component(e, Destination(p.x, p.y + 64, c.speed))            
                p.rotation = MOVE_DOWN
                p.walk = True
            elif self.move == MOVE_LEFT:
                if p.x > 63 and not 'blocked' in themap.get_at(p.x - 64, p.y):
                    entities.add_component(e, Destination(p.x - 64, p.y, c.speed))
                p.rotation = MOVE_LEFT
                p.walk = True
            elif self.move == MOVE_RIGHT:
                if p.x < 897 and not 'blocked' in themap.get_at(p.x + 64, p.y):
                    entities.add_component(e, Destination(p.x + 64, p.y, c.speed))
                p.rotation = MOVE_RIGHT
                p.walk = True
            else:
                p.walk = False

class FoodSystem(ecs.System):
    def update(self, dt, entities):
        self.updateIndicators(dt, entities)
        pairs = entities.pairs_for_type(Bug)
        if pairs == None: return # no bugs left!?
        (e, c) = pairs[0]
        themaps = entities.pairs_for_type(Map)
        if themaps == None: return
        (e2, mappus) = themaps[0]
        themap = mappus.map
        p = entities.component_for_entity(e, Physics)
        d = entities.component_for_entity(e, Destination)
        c.food-=0.08
        if c.food < 0: c.food = 0
        if not d: # if the roach stays still
            tile = themap.get_at(p.x, p.y)
            if 'current_food' in tile:
                if tile['current_food'] > 0 and c.food < c.max_food:
                    if not c.eating:
                        a = entities.new_entity()
                        entities.add_component(a, Physics(p.x, p.y))
                        entities.add_component(a, Effect(size=100))
                        entities.add_component(c, Sound('nom'))
                        c.eating = True
                    tile['current_food'] -= 0.5
                    if tile['current_food'] < 0: tile['current_food'] = 0
                    c.food += 0.5
                else:
                    c.eating = False
            else:
                c.eating = False
        else:
            c.eating = False
        if c.eating == False:
            entities.remove_component(c, Sound)

    def updateIndicators(self, dt, entities):
        pairs = entities.pairs_for_type(FoodIndicator)
        if not pairs: return
        for (e, c) in pairs:
            c.food = c.tile['current_food']
                
class DestinationSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Destination)
        if not pairs: return
        for (e, c) in pairs:
            dx = 0
            dy = 0
            phy = entities.component_for_entity(e, Physics)
            dx = (c.x - phy.x)
            dy = (c.y - phy.y)
            dist = sqrt(dx*dx + dy*dy)
            if dist < c.speed:
                entities.remove_component(e, Destination)
                phy.x = c.x
                phy.y = c.y
                phy.dx = 0
                phy.dy = 0
                return
            dx = dx/dist * c.speed
            dy = dy/dist * c.speed
            phy.dx = dx
            phy.dy = dy

class PhysicsSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Physics)
        if not pairs: return
        for (e, c) in pairs:
            c.x += c.dx
            c.y += c.dy
            
class TitleScreen(ecs.System):
    def __init__(self):
        self.ran = False
        self.dt = 0
        
    def update(self, dt, entities):
        if not self.ran:
            a = entities.new_entity()
            entities.add_component(a, StaticImage("title.png", (0,0), BOTTOMRIGHT))
            entities.add_component(a, Transient())
            entities.add_component(a, TTL(1500))

            a = entities.new_entity()
            entities.add_component(a, Physics(1024/2,768/2))
            entities.add_component(a, Effect(size=1000))
            entities.add_component(a, Transient())            
            entities.add_component(a, TTL(1500))

            a = entities.new_entity()
            entities.add_component(a, Transient())
            entities.add_component(a, Sound('nom'))
            self.ran = True
        self.dt += dt
#        if self.dt > 1500:
#            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))
    def getCallbacks(self):
        return [
            EventHandler(pygame.KEYDOWN, self.handleKeyDown),
        ]

    def handleKeyDown(self, entities, event):
        if event.key == pygame.K_SPACE:
            self.pic = None
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))

class WinScreen(ecs.System):
    def __init__(self):
        self.last = 0
        
    def update(self, dt, entities):
        self.last += dt
        if self.last>(1000*random.random()):
            a = entities.new_entity()
            entities.add_component(a, Physics(random.randint(0,1024), random.randint(0,768)))
            entities.add_component(a, Effect(size=random.randint(0,400)))
            entities.add_component(a, TTL(1000))
            self.last = 0

class LoaderSystem(ecs.System):
    def __init__(self, load, mapname):
        self.ran = False
        self.load = load
        self.mapname = mapname

    def stateChanged(self, entities):
        entities.database = {} # clear all evarything
        self.ran = False
        
    def update(self, dt, entities):
        if not self.ran:
            self.ran = True
            m = self.load(self.mapname)
            pairs = entities.pairs_for_type(Map)
            if pairs == None:
                e = entities.new_entity()
                entities.add_component(e, Map(m))
            else:
                (e, c) = pairs[0]
                c.map = m
            pairs = entities.pairs_for_type(Bug)
            if pairs == None:
                e = entities.new_entity()
                entities.add_component(e, Bug())
                e2 = entities.new_entity()
                sh = Showerhead()
                entities.add_component(e2, sh)
            else:
                (e, c) = pairs[0]
            pairs = entities.pairs_for_type(Showerhead)
            if pairs == None:
                e2 = entities.new_entity()
                sh = Showerhead()
                entities.add_component(e2, sh)
            else:
                (e2, sh) = pairs[0]

            entities.add_component(e, Visual('roach'))
            entities.add_component(e, Physics(0,0))
            entities.add_component(e2, Visual('showerhead'))
            entities.add_component(e2, Physics(sh.start_x,sh.start_y))

            for tt in m.find('base_food'):
                a = entities.new_entity()
                c = FoodIndicator(tt)
                entities.add_component(a, c)
            
            music = entities.new_entity()
            entities.add_component(music, Music("Zeropage_-_Ambient_Dance"))
        pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))

class ScoreScreenSystem(ecs.System):
    """ The score screen """
    def __init__(self):
        self.new = True
        self.pic = None

    def stateChanged(self, entities):
        self.new = True
        
    def update(self, dt, entities):
        if self.new:
            self.new = False
            self.pic = entities.new_entity()
            entities.add_component(self.pic, Transient())        
            entities.add_component(self.pic, StaticImage('end.png', (1024/2,300)))

            pairs = entities.pairs_for_type(Score)
            if pairs != None:
                (e, c) = pairs[0]
                entities.add_component(self.pic, OnScreenText("%i points" % c.score))
        
    def getCallbacks(self):
        return [
            EventHandler(pygame.KEYDOWN, self.handleKeyDown),
        ]

    def handleKeyDown(self, entities, event):
        if event.key == pygame.K_SPACE:
            self.pic = None
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))
        
class PauseSystem(ecs.System):
    def __init__(self):
        self.new = True
        self.pic = None

    def stateChanged(self, entities):
        self.new = True
        
    def update(self, dt, entities):
        if self.new:
            self.new = False
            self.pic = entities.new_entity()
#            entities.add_component(self.pic, Physics(1024/2,300))
            entities.add_component(self.pic, Transient())            
#            entities.add_component(self.pic, Effect(size=500,image="title.png"))
            entities.add_component(self.pic, StaticImage('pause.png', (1024/2,300)))
        
    def getCallbacks(self):
        return [
            EventHandler(pygame.KEYDOWN, self.handleKeyDown),
        ]

    def handleKeyDown(self, entities, event):
        if event.key == pygame.K_SPACE:
            self.pic = None
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))
        
class DebugSystem(ecs.System):
    def update(self, dt, entities):
        print("Update after %f" % dt)

class ScoringSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Score)
        if not pairs:
            c = entities.new_entity()
            s = Score()
            entities.add_component(c, s)
        else:
            (c, s) = pairs[0]
        s.score += dt

class Game(object):
    def __init__(self):
        self.states = { 
            "title": ["render", "sound", "titlescreen", "ttl"],
            "pause": ["render", "sound", "pause"],
            "run":   ["render", "sound", "usermovement", "showermovement", "food", "destination", "physics", "ttl", "wet", "goal", "scoring", "poison"],
            "loader":["render", "sound", "maploader"],
            "score": ["render", "sound", "scorescreen", "physics"],
            "ended": ["render", "sound", "ttl"]
        }
        
        self.state = "title"

        self.eng = pygameEngine()
        self.entity  = ecs.EntityManager()
        
        self.systems = {
            "maploader": LoaderSystem(self.eng.load_map, "shower"),
            "titlescreen": TitleScreen(),
            "winscreen": WinScreen(),
            "render": self.eng.renderer(),
            "sound": self.eng.sounder(),
            "usermovement": UserMovement(),
            "physics": PhysicsSystem(),
            "destination": DestinationSystem(),            
            "ttl": TTLSystem(),
            "pause": PauseSystem(),
            "scorescreen": ScoreScreenSystem(),
            "scoring": ScoringSystem(),            
            "showermovement": ShowerMovement(),
            "food": FoodSystem(),
            "goal": GoalSystem(),
            "wet": WetSystem(),
            "poison": PoisonSystem()
        }

        self.statetrans = {
            "run": "score",
            "pause": "run",
            "ended": "title",
            "title": "loader",
            "score": "loader",
            "loader": "run"
        }

        self.eventhandlers = {}
        self.machines={}
        for k in self.states.keys():
            self.machines[k] = ecs.SystemManager(self)
            self.eventhandlers[k]=[]
            for v in self.states[k]:
                self.machines[k].add_system(self.systems[v])
                try:
                    foo = self.systems[v].getCallbacks()
                except AttributeError:
                    continue
                self.eventhandlers[k].extend(foo)

        # Setup Event Handling
        for k,v in self.eventhandlers.iteritems():
            v.extend([ EventHandler([pygame.KEYDOWN, pygame.KEYUP], self.handleKeyPress),
                       EventHandler(pygame.QUIT, self.handleQuitEvent),
                       EventHandler(pygame.USEREVENT, self.handleUserEvent)
                     ])

        self.populate()
        self.clock = self.eng.clocker()
        self.running=True
        
    def events(self):
        for event in pygame.event.get():
            for c in self.eventhandlers[self.state]:
                if type(c.event) == list:
                    if event.type in c.event:
                        c.cbHndl(self.entity, event)
                elif c.event == event.type:
                    c.cbHndl(self.entity, event)        

    def run(self):
        while self.running:
            self.machines[self.state].update_systems(self.clock.tick(40), self.entity)
            self.events()

    def statetransition(self, newstate=""):
        # should be done in a saner place than here, I guess
        if newstate == "": newstate = self.statetrans[self.state]
        self.state = newstate
        self.deleteTransientEntities()
        for s in self.states[newstate]:
            try:
                self.systems[s].stateChanged(self.entity)
            except AttributeError:
                pass
        
    def populate(self):
        a = self.entity.new_entity()
        self.entity.add_component(a, Mold())
        pass
    
    def handleUserEvent(self, entities, event):
        if event.dict['msg'] == EVT_STATE:
            s = event.dict.get('state', '')
            self.statetransition(s)

    def handleKeyPress(self, entities, event):
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            self.running = False

    def handleQuitEvent(self, entities, event):
        self.running=False

    def deleteTransientEntities(self):
        pairs = self.entity.pairs_for_type(Transient)
        if pairs == None: return
        for (e, c) in pairs:
            self.entity.delete_entity(e)

def main():
        g = Game()
        g.run()

main()
