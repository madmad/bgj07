<?xml version="1.0" encoding="UTF-8"?>
<tileset name="shower_tiles" tilewidth="64" tileheight="64" spacing="1" margin="1">
 <properties>
  <property name="margin" value="1"/>
  <property name="spacing" value="1"/>
 </properties>
 <image source="assets/shower_tiles.png" trans="ff00ff" width="265" height="199"/>
 <tile id="0">
  <properties>
   <property name="regular" value=""/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="empty" value=""/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="spawn1" value=""/>
  </properties>
 </tile>
</tileset>
